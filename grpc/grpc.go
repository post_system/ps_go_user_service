package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"post_system/ps_go_user_service/config"
	"post_system/ps_go_user_service/genproto/user_service"
	"post_system/ps_go_user_service/grpc/client"
	"post_system/ps_go_user_service/grpc/service"
	"post_system/ps_go_user_service/pkg/logger"
	"post_system/ps_go_user_service/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	user_service.RegisterAdminUserServiceServer(grpcServer, service.NewUserService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
